package uz.pdp.springjpawarehouseproject.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.springjpawarehouseproject.enums.CustomFilterType;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqProductFilter {
    private boolean priceDesc;
    private CustomFilterType customFilterType;
    private String name;
}
