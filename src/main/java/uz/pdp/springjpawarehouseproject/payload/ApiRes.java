package uz.pdp.springjpawarehouseproject.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiRes {
    private String message;
    private boolean success;
    private Object object;
    private Long totalElements;

    public ApiRes(String message, boolean success, Object object) {
        this.message = message;
        this.success = success;
        this.object = object;
    }

    public ApiRes(String message, boolean success) {
        this.message = message;
        this.success = success;
    }
}
