package uz.pdp.springjpawarehouseproject.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AttachmentContent {
    @Id
    @GeneratedValue
    private UUID id;

    private byte[] bytes;

    @OneToOne(fetch = FetchType.LAZY,optional = false)
    private Attachment attachment;
}
