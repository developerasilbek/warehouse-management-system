package uz.pdp.springjpawarehouseproject.enums;

public enum CustomFilterType {
    STARTS_WITH,
    CONTAINS,
    ENDS_WITH
}
