package uz.pdp.springjpawarehouseproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaWarehouseProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJpaWarehouseProjectApplication.class, args);
    }

}
