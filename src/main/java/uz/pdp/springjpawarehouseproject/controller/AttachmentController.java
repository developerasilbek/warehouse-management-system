package uz.pdp.springjpawarehouseproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.springjpawarehouseproject.payload.ApiRes;
import uz.pdp.springjpawarehouseproject.service.AttachmentService;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @PostMapping(value = "/uploadToDB")
    public HttpEntity<?> uploadToDB(MultipartHttpServletRequest multipartHttpServletRequest) throws IOException {
        ApiRes response = attachmentService.uploadToDB(multipartHttpServletRequest);
        return ResponseEntity.status(response.isSuccess() ? 201 : 409).body(response);
    }

    @GetMapping(value = "/downloadFromDB/{id}")
    public HttpEntity<?> downloadFromDB(@PathVariable UUID id){
        return attachmentService.downloadFromDB(id);
    }


}
