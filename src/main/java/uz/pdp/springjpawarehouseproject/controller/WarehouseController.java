package uz.pdp.springjpawarehouseproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.springjpawarehouseproject.entity.Warehouse;
import uz.pdp.springjpawarehouseproject.payload.ApiRes;
import uz.pdp.springjpawarehouseproject.payload.ReqWarehouseFilter;
import uz.pdp.springjpawarehouseproject.service.WarehouseService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/warehouse")
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;

    @PostMapping(value = "/save")
    public HttpEntity<?> save(@RequestBody Warehouse warehouse) {
        ApiRes apiRes = warehouseService.save(warehouse);
        return ResponseEntity.status(apiRes.isSuccess() ? 201 : 409).body(apiRes);
    }

    @PostMapping(value = "/edit/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id, @RequestBody @Valid Warehouse warehouse) {
        if (id != null) {
            ApiRes byId = warehouseService.getById(id);
            Warehouse warehouseOld = (Warehouse) byId.getObject();
            warehouseOld.setName(warehouse.getName());
            warehouseOld.setAddress(warehouse.getAddress());
            warehouseService.save(warehouseOld);
            return ResponseEntity.status(byId.isSuccess()?201:409).body(byId);
        }
        return ResponseEntity.status(409).body(new ApiRes("warehouseId can not be null",false));
    }

    @DeleteMapping(value = "/delete/{id}")
    public HttpEntity<?> deleteById(@PathVariable Integer id){
        ApiRes apiRes = warehouseService.removeById(id);
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @GetMapping(value = "/all")
    public HttpEntity<?> getAll(){
        ApiRes apiRes = warehouseService.getAll();
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @GetMapping(value = "/byId/{id}")
    public HttpEntity<?> getById(@PathVariable Integer id){
        ApiRes apiRes = warehouseService.findById(id);
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @GetMapping(value = "/page")
    public HttpEntity<?> getByPage(@RequestParam int page,
                                   @RequestParam int size){
        ApiRes apiRes = warehouseService.getByPage(page,size);
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

//    @PostMapping(value = "/filter")
//    public HttpEntity<?> getByFilter(@RequestBody ReqWarehouseFilter reqWarehouseFilter){
//       ApiRes apiRes = warehouseService.filter(reqWarehouseFilter);
//       return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
//    }
}
