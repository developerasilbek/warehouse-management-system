package uz.pdp.springjpawarehouseproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.springjpawarehouseproject.entity.Product;
import uz.pdp.springjpawarehouseproject.payload.ApiRes;
import uz.pdp.springjpawarehouseproject.payload.ReqProductFilter;
import uz.pdp.springjpawarehouseproject.service.ProductService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping(value = "/save")
    public HttpEntity<?> save(@RequestBody Product product) {
        ApiRes apiRes = productService.save(product);
        return ResponseEntity.status(apiRes.isSuccess() ? 201 : 409).body(apiRes);
    }

    @PostMapping(value = "/edit/{id}")
    public HttpEntity<?> edit(@PathVariable Integer id, @RequestBody @Valid Product product) {
        if (id != null) {
            ApiRes byId = productService.getById(id);
            Product productOld = (Product) byId.getObject();
            productOld.setName(product.getName());
            productOld.setPrice(product.getPrice());
            productService.save(productOld);
            return ResponseEntity.status(byId.isSuccess()?201:409).body(byId);
        }
        return ResponseEntity.status(409).body(new ApiRes("productId can not be null",false));
    }

    @DeleteMapping(value = "/delete/{id}")
    public HttpEntity<?> deleteById(@PathVariable Integer id){
        ApiRes apiRes = productService.removeById(id);
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @GetMapping(value = "/all")
    public HttpEntity<?> getAll(){
        ApiRes apiRes = productService.getAll();
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @GetMapping(value = "/byId/{id}")
    public HttpEntity<?> getById(@PathVariable Integer id){
        ApiRes apiRes = productService.findById(id);
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @GetMapping(value = "/page")
    public HttpEntity<?> getByPage(@RequestParam int page,
                                   @RequestParam int size){
        ApiRes apiRes = productService.getByPage(page,size);
        return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }

    @PostMapping(value = "/filter")
    public HttpEntity<?> getByFilter(@RequestBody ReqProductFilter reqProductFilter){
       ApiRes apiRes = productService.filter(reqProductFilter);
       return ResponseEntity.status(apiRes.isSuccess()?201:409).body(apiRes);
    }
}
