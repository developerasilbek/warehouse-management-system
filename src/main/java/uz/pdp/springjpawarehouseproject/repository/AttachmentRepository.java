package uz.pdp.springjpawarehouseproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.springjpawarehouseproject.entity.Attachment;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
}
