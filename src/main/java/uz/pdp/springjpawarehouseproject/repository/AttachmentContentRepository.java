package uz.pdp.springjpawarehouseproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.springjpawarehouseproject.entity.Attachment;
import uz.pdp.springjpawarehouseproject.entity.AttachmentContent;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {

        AttachmentContent findByAttachment(Attachment attachment);

}
