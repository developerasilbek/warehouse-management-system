package uz.pdp.springjpawarehouseproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.springjpawarehouseproject.entity.Product;
import uz.pdp.springjpawarehouseproject.payload.ReqProductFilter;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Integer> {
    List<Product> findAllByNameContainingOrderByPrice(String name);
    List<Product> findAllByNameContainingOrderByPriceDesc(String name);

    List<Product> findAllByNameStartsWithOrderByPrice(String name);
    List<Product> findAllByNameStartsWithOrderByPriceDesc(String name);

    List<Product> findAllByNameEndingWithOrderByPrice(String name);
    List<Product> findAllByNameEndingWithOrderByPriceDesc(String name);
}
