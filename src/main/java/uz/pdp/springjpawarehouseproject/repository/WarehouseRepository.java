package uz.pdp.springjpawarehouseproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.springjpawarehouseproject.entity.Product;
import uz.pdp.springjpawarehouseproject.entity.Warehouse;

import java.util.List;


public interface WarehouseRepository extends JpaRepository<Warehouse,Integer> {

//    List<Warehouse> findAllByNameContainingOrderByPrice(String name);
//    List<Warehouse> findAllByNameContainingOrderByPriceDesc(String name);
//
//    List<Warehouse> findAllByNameStartsWithOrderByPrice(String name);
//    List<Warehouse> findAllByNameStartsWithOrderByPriceDesc(String name);
//
//    List<Warehouse> findAllByNameEndingWithOrderByPrice(String name);
//    List<Warehouse> findAllByNameEndingWithOrderByPriceDesc(String name);

}
