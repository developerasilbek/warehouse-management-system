package uz.pdp.springjpawarehouseproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import uz.pdp.springjpawarehouseproject.entity.Warehouse;
import uz.pdp.springjpawarehouseproject.payload.ApiRes;
import uz.pdp.springjpawarehouseproject.payload.ReqWarehouseFilter;
import uz.pdp.springjpawarehouseproject.repository.WarehouseRepository;
import uz.pdp.springjpawarehouseproject.repository.WarehouseRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class WarehouseService {
    @Autowired
    WarehouseRepository warehouseRepository;

    public ApiRes save(Warehouse warehouse) {
        warehouseRepository.save(warehouse);
        return new ApiRes("saved", true);
    }

    public ApiRes getById(Integer id) {
        Optional<Warehouse> byId = warehouseRepository.findById(id);
        return new ApiRes("edited", true, byId.get());
    }

    public ApiRes removeById(Integer id) {
        warehouseRepository.deleteById(id);
        return new ApiRes("removed", true);
    }

    public ApiRes getAll() {
        List<Warehouse> all = warehouseRepository.findAll();
        return new ApiRes("seccess", true, all);
    }

    public ApiRes findById(Integer id) {
        Optional<Warehouse> byId = warehouseRepository.findById(id);
        if (byId.isPresent()) {
            Warehouse warehouse = byId.get();
            return new ApiRes("Warehouse By id", true, warehouse);
        }
        return new ApiRes("Such WarehouseId doesn't exist", false);
    }

    public ApiRes getByPage(int page, int size) {
        Page<Warehouse> all = warehouseRepository.findAll(PageRequest.of(page, size, Sort.by("id")));
        return new ApiRes("pages", true, all);
    }

//    public ApiRes filter(ReqWarehouseFilter reqWarehouseFilter) {
//        List<Warehouse> warehouses = new ArrayList<>();
//        switch (reqWarehouseFilter.getCustomFilterType()) {
//            case CONTAINS:
//                if (reqWarehouseFilter.isPriceDesc()) {
//                    warehouses = warehouseRepository.findAllByNameContainingOrderByPriceDesc(reqWarehouseFilter.getName());
//                } else {
//                    warehouses = warehouseRepository.findAllByNameContainingOrderByPrice(reqWarehouseFilter.getName());
//                }
//                break;
//            case ENDS_WITH:
//                if (reqWarehouseFilter.isPriceDesc()) {
//                    warehouses = warehouseRepository.findAllByNameEndingWithOrderByPriceDesc(reqWarehouseFilter.getName());
//                } else {
//                    warehouses = warehouseRepository.findAllByNameEndingWithOrderByPrice(reqWarehouseFilter.getName());
//                }
//                break;
//            case STARTS_WITH:
//                if (reqWarehouseFilter.isPriceDesc()) {
//                    warehouses = warehouseRepository.findAllByNameStartsWithOrderByPriceDesc(reqWarehouseFilter.getName());
//                } else {
//                    warehouses = warehouseRepository.findAllByNameStartsWithOrderByPrice(reqWarehouseFilter.getName());
//                }
//                break;
//        }
//        return new ApiRes("sorted warehouses", true, warehouses);
//    }
}
