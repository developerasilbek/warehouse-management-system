package uz.pdp.springjpawarehouseproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.springjpawarehouseproject.entity.Attachment;
import uz.pdp.springjpawarehouseproject.entity.AttachmentContent;
import uz.pdp.springjpawarehouseproject.payload.ApiRes;
import uz.pdp.springjpawarehouseproject.repository.AttachmentContentRepository;
import uz.pdp.springjpawarehouseproject.repository.AttachmentRepository;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
public class AttachmentService {
    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Transactional
    public ApiRes uploadToDB(MultipartHttpServletRequest multipartHttpServletRequest) throws IOException {
        MultipartFile file = multipartHttpServletRequest.getFile("file");
        Attachment attachment = new Attachment();
        attachment.setName(file.getName());
        attachment.setOriginalName(file.getOriginalFilename());
        attachment.setContentType(file.getContentType());
        attachment.setSize(file.getSize());
        attachmentRepository.saveAndFlush(attachment);
        AttachmentContent attachmentContent = new AttachmentContent();
        attachmentContent.setAttachment(attachment);
        attachmentContent.setBytes(file.getBytes());
        attachmentContentRepository.save(attachmentContent);
        return new ApiRes("Saved", true);
    }


    public ResponseEntity<?> downloadFromDB(UUID id) {
        Optional<Attachment> optionalAttachment = attachmentRepository.findById(id);
        if (optionalAttachment.isPresent()) {
            Attachment attachment = optionalAttachment.get();
            AttachmentContent content = attachmentContentRepository.findByAttachment(attachment);
            return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attechment; fileName=\"" + attachment.getOriginalName() + "\"")
                    .body(content.getBytes());
        }
        return ResponseEntity.status(400).body("Topilmadi uzur!!!");
    }
}
