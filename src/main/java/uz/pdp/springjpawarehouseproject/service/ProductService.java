package uz.pdp.springjpawarehouseproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Service;
import uz.pdp.springjpawarehouseproject.entity.Product;
import uz.pdp.springjpawarehouseproject.payload.ApiRes;
import uz.pdp.springjpawarehouseproject.payload.ReqProductFilter;
import uz.pdp.springjpawarehouseproject.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public ApiRes save(Product product) {
        productRepository.save(product);
        return new ApiRes("saved", true);
    }

    public ApiRes getById(Integer id) {
        Optional<Product> byId = productRepository.findById(id);
        return new ApiRes("edited", true, byId.get());
    }

    public ApiRes removeById(Integer id) {
        productRepository.deleteById(id);
        return new ApiRes("removed", true);
    }

    public ApiRes getAll() {
        List<Product> all = productRepository.findAll();
        return new ApiRes("seccess", true, all);
    }

    public ApiRes findById(Integer id) {
        Optional<Product> byId = productRepository.findById(id);
        if (byId.isPresent()) {
            Product product = byId.get();
            return new ApiRes("Product By id", true, product);
        }
        return new ApiRes("Such ProductId doesn't exist", false);
    }

    public ApiRes getByPage(int page, int size) {
        Page<Product> all = productRepository.findAll(PageRequest.of(page, size, Sort.by("id")));
        return new ApiRes("pages", true, all);
    }

    public ApiRes filter(ReqProductFilter reqProductFilter) {
        List<Product> products = new ArrayList<>();
        switch (reqProductFilter.getCustomFilterType()) {
            case CONTAINS:
                if (reqProductFilter.isPriceDesc()) {
                    products = productRepository.findAllByNameContainingOrderByPriceDesc(reqProductFilter.getName());
                } else {
                    products = productRepository.findAllByNameContainingOrderByPrice(reqProductFilter.getName());
                }
                break;
            case ENDS_WITH:
                if (reqProductFilter.isPriceDesc()) {
                    products = productRepository.findAllByNameEndingWithOrderByPriceDesc(reqProductFilter.getName());
                } else {
                    products = productRepository.findAllByNameEndingWithOrderByPrice(reqProductFilter.getName());
                }
                break;
            case STARTS_WITH:
                if (reqProductFilter.isPriceDesc()) {
                    products = productRepository.findAllByNameStartsWithOrderByPriceDesc(reqProductFilter.getName());
                } else {
                    products = productRepository.findAllByNameStartsWithOrderByPrice(reqProductFilter.getName());
                }
                break;
        }
        return new ApiRes("sorted products", true, products);
    }
}
